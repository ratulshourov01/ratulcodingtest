<?php

namespace App\Http\Controllers;

use App\User;
use App\UserActivate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use Stripe;

class StripePaymentController extends Controller
{
    public function stripe()
    {
        return view('stripe');
    }
  

  

    /**

     * success response method.

     *

     * @return \Illuminate\Http\Response

     */

    public function stripePost(Request $request)

    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        Stripe\Charge::create ([

                "amount" => 100*10,

                "currency" => "usd",

                "source" => $request->stripeToken,

                "description" => "Successfully Activation For 1 month" 

        ]);
            $activate=new UserActivate();
            $activate->user_id=1;
            $activate->amount="$10USD";
            $activate->days=30;
            $activate->flag=1;
            $activate->save();
        Session::flash('success', 'Payment successful!');

          

        return redirect('/home');

    }
}
