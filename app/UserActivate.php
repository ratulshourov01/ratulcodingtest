<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivate extends Model
{
    protected $fillable=['user_id','amount','days','flag'];
    //
}
